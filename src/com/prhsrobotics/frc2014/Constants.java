package com.prhsrobotics.frc2014;

import com.prhsrobotics.frc2014.lib.util.ConstantsBase;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class Constants extends ConstantsBase{

    // drive motors
    public static final Constant front_left_drive_motor = new Constant("front_left_drive_motor", "3");
    public static final Constant rear_left_drive_motor = new Constant("rear_left_drive_motor", "4");
    public static final Constant front_right_drive_motor = new Constant("front_right_drive_motor", "2");
    public static final Constant rear_right_drive_motor = new Constant("rear_right_drive_motor", "1");

    // joysticks
    public static final Constant driver_joystick = new Constant("driver_joystick", "1");
    public static final Constant manipulator_joystick = new Constant("driver_joystick", "2");

    // joystick sticks
    public static final Constant left_stick_x = new Constant("left_stick_x", "1");
    public static final Constant left_stick_y = new Constant("left_stick_y", "2");
    public static final Constant triggers = new Constant("triggers", "3");
    public static final Constant right_stick_x = new Constant("right_stick_x", "4");
    public static final Constant right_stick_y = new Constant("right_stick_y", "5");
    public static final Constant d_pad_left_right = new Constant("d_pad_left_right", "6");

    //joystick buttons
    public static final Constant button_a = new Constant("button_a", "1"); //button A
    public static final Constant button_b = new Constant("button_b", "2"); //button B
    public static final Constant button_x = new Constant("button_x", "3"); //button X
    public static final Constant button_y = new Constant("button_y", "4"); //button Y
    public static final Constant left_bumper = new Constant("left_bumper", "5");
    public static final Constant right_bumper = new Constant("right_bumper", "6");

    //network tables
    public static final Constant axis_camera_table = new Constant("axis_camera_table", "axis_camera");

    //network table variable names
    public static final Constant boolean_camera_detect = new Constant("boolean_camera_detect", "cam_detected");
    public static final Constant double_camera_distance = new Constant("double_camera_distance", "cam_distance");

    //encoders
    public static final Constant left_encoder_port_a = new Constant("left_encoder_port_a", "1");
    public static final Constant left_encoder_port_b = new Constant("left_encoder_port_b", "2");
    public static final Constant right_encoder_port_a = new Constant("right_encoder_port_a", "3");
    public static final Constant right_encoder_port_b = new Constant("right_encoder_port_b", "4");

    //gyroscope
    public static final Constant gyro_port = new Constant("gyro_port", "1");

    //drive tuning values
    public static final Constant straight_kP = new Constant("straight_kP", ".045");
    public static final Constant straight_kI = new Constant("straight_kI", "0");
    public static final Constant straight_kD = new Constant("straight_kD", ".17");
    public static final Constant turn_kP = new Constant("turn_kP", ".045");
    public static final Constant turn_kI = new Constant("turn_kI", "0");
    public static final Constant turn_kD = new Constant("turn_kD", ".15");

    //solenoids
    public static final Constant left_solenoid_a = new Constant("left_solenoid_a_port", "1");
    public static final Constant left_solenoid_b = new Constant("left_solenoid_b_port", "2");
    public static final Constant right_solenoid_a = new Constant("right_solenoid_a_port", "3");
    public static final Constant right_solenoid_b = new Constant("right_solenoid_b_port", "4");
    public static final Constant intake_solenoid_a = new Constant("intake_solenoid_a", "6");
    public static final Constant intake_solenoid_b = new Constant("intake_solenoid_b", "5");

    //intake motor
    public static final Constant intake_motor_port = new Constant("intake_motor_port", "5");

    //defense arm
    public static final Constant defense_motor = new Constant("defense_motor_port", "6");

    //compressor
    public static final Constant compressor_pressure_switch = new Constant("compressor_pressure_switch", "14");
    public static final Constant compressor_port = new Constant("compressor_port", "1");

    //sensors
    public static final Constant light_sensor_intake = new Constant("light_sensor_intake", "13");
    public static final Constant light_sensor_held = new Constant("light_sensor_held", "12");
    
    //states
    public static final Constant intake_motor_state= new Constant("intake_motor_state", "0");
    public static final Constant ball_held_state = new Constant("ball_held_state", "0");
    public static final Constant intake_position = new Constant("intake_position", "0");
    public static final Constant automated_state = new Constant("automated_state", "0");
}
