
package com.prhsrobotics.frc2014;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.buttons.Button;
import com.prhsrobotics.frc2014.commands.ShooterPull;
import com.prhsrobotics.frc2014.commands.ShooterPush;
import com.prhsrobotics.frc2014.commands.IntakePull;
import com.prhsrobotics.frc2014.commands.IntakePush;
import com.prhsrobotics.frc2014.commands.IntakeDrawIn;
import com.prhsrobotics.frc2014.commands.IntakeDrawOut;
import com.prhsrobotics.frc2014.commands.DefenseDown;
import com.prhsrobotics.frc2014.commands.DefenseUp;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    public static Joystick driverJoystick = new Joystick(Constants.driver_joystick.getInt());
    public static Joystick manipulatorJoystick = new Joystick(Constants.manipulator_joystick.getInt());
    public static Button intakeInButton = new JoystickButton(manipulatorJoystick, Constants.button_x.getInt());
    public static Button intakenDrawIn = new JoystickButton(manipulatorJoystick, Constants.button_y.getInt());
    public static Button shooterPushButton = new JoystickButton(manipulatorJoystick, Constants.button_a.getInt());
    public static Button shooterPullButton = new JoystickButton(manipulatorJoystick, Constants.button_b.getInt());
    public static Button intakePullButton = new JoystickButton(manipulatorJoystick, Constants.left_bumper.getInt());
    public static Button intakePushButton = new JoystickButton(manipulatorJoystick, Constants.right_bumper.getInt());
    public static Button defenseDownButton = new JoystickButton(manipulatorJoystick, 7); //this value need to change
    public static Button defenseUpButton = new JoystickButton(manipulatorJoystick, 8);   //this value needs to cahnge
    public OI()
    {
        intakeInButton.whileHeld(new IntakeDrawIn());
        intakenDrawIn.whileHeld(new IntakeDrawOut());
        shooterPushButton.whileHeld(new ShooterPush());
        shooterPullButton.whileHeld(new ShooterPull());
        intakePullButton.whileHeld(new IntakePull());
        intakePushButton.whileHeld(new IntakePush());
        defenseDownButton.whileHeld(new DefenseDown());
        defenseUpButton.whileHeld(new DefenseUp());
    }
    //// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    // Joystick stick = new Joystick(port);
    // Button button = new JoystickButton(stick, buttonNumber);
    
    // Another type of button you can create is a DigitalIOButton, which is
    // a button or switch hooked up to the cypress module. These are useful if
    // you want to build a customized operator interface.
    // Button button = new DigitalIOButton(1);
    
    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.
    
    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:
    
    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());
    
    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());
    
    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());
}

