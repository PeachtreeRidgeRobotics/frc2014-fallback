/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

import com.prhsrobotics.frc2014.Constants;

/**
 *
 * @author RoboLions
 */
public class IntakeDrawOut extends CommandBase {
    
    public IntakeDrawOut() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(intake);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        Constants.intake_motor_state.setVal(1);
        intake.draw_out();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        intake.draw_stop();
        Constants.intake_motor_state.setVal(0);
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        intake.draw_stop();
        Constants.intake_motor_state.setVal(0);
    }
}
