/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

import com.prhsrobotics.frc2014.Constants;

/**
 *
 * @author Rikhil
 */
public class AutoIntake extends CommandBase {
    
    public AutoIntake() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(sensors);
        requires(intake);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    if(Constants.automated_state.getInt()==0){
        if(Constants.intake_motor_state.getInt()==0 && Constants.ball_held_state.getInt()==0 && Constants.intake_position.getInt() == 1)
        {
            if(sensors.getLightSensorIntakeState()){
                intake.draw_in();
                intake.pull();
            }
            if(sensors.getLightSensorHeldState()){
                intake.draw_stop();
                intake.pull_stop();
                Constants.ball_held_state.setVal(1);
                Constants.intake_position.setVal(0);
            }
        }
        if(!sensors.getLightSensorHeldState()){
            Constants.ball_held_state.setVal(0);
        }
    }    
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
