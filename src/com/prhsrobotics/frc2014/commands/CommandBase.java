package com.prhsrobotics.frc2014.commands;

import edu.wpi.first.wpilibj.command.Command;
import com.prhsrobotics.frc2014.OI;
import com.prhsrobotics.frc2014.subsystems.Chassis;
import com.prhsrobotics.frc2014.subsystems.ChooseAutomation;
import com.prhsrobotics.frc2014.subsystems.CompressorSystem;
import com.prhsrobotics.frc2014.subsystems.DefensiveArm;
import com.prhsrobotics.frc2014.subsystems.Intake;
import com.prhsrobotics.frc2014.subsystems.Sensors;
import com.prhsrobotics.frc2014.subsystems.Shooter;
/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 * @author Author
 */
public abstract class CommandBase extends Command {

    public static OI oi;
    // Create a single static instance of all of your subsystems
    public static Chassis chassis = new Chassis();
    public static CompressorSystem compressor = new CompressorSystem();
    public static Shooter shoot = new Shooter();
    public static Intake intake = new Intake();
    public static DefensiveArm defensearm = new DefensiveArm();
    public static Sensors sensors = new Sensors();
    public static ChooseAutomation chooseautomation = new ChooseAutomation();
    
    public static void init() {
        // This MUST be here. If the OI creates Commands (which it very likely
        // will), constructing it during the construction of CommandBase (from
        // which commands extend), subsystems are not guaranteed to be
        // yet. Thus, their requires() statements may grab null pointers. Bad
        // news. Don't move it.
        oi = new OI();

        // Show what command your subsystem is running on the SmartDashboard
    }

    public CommandBase(String name) {
        super(name);
    }

    public CommandBase() {
        super();
    }
}
