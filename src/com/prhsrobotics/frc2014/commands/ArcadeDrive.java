/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author Rohil
 */
public class ArcadeDrive extends CommandBase{
    
    public ArcadeDrive(){
        requires(chassis);
    }
    
    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        chassis.drivetrain.arcadeDrive(oi.driverJoystick, Constants.left_stick_y.getInt(), oi.driverJoystick , Constants.right_stick_x.getInt());
        SmartDashboard.putNumber("Front Left Motor", Constants.front_left_drive_motor.getInt());
        SmartDashboard.putNumber("Front Right Motor", Constants.front_right_drive_motor.getInt());
        SmartDashboard.putNumber("Rear Left Motor", Constants.rear_left_drive_motor.getInt());
        SmartDashboard.putNumber("Rear Right Motor", Constants.rear_right_drive_motor.getInt());
        SmartDashboard.putNumber("Battery", DriverStation.getInstance().getBatteryVoltage());
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        chassis.brake();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        chassis.brake();
    }
}
