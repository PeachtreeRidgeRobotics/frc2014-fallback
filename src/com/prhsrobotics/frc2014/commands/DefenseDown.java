/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

/**
 *
 * @author RoboLions
 */
public class DefenseDown extends CommandBase {
    
    public DefenseDown() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(defensearm);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        defensearm.defense_down();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        defensearm.defense_stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        defensearm.defense_stop();
    }
}
