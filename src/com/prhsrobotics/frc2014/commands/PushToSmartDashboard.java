/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.commands;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author RoboLions
 */
public class PushToSmartDashboard extends CommandBase {
    
    public PushToSmartDashboard() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(sensors);
        requires(chooseautomation);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        SmartDashboard.putBoolean("Intake Light Sensor: ", sensors.getLightSensorIntakeState());
        SmartDashboard.putBoolean("Held Light Sensor", sensors.getLightSensorHeldState());
        Constants.automated_state.setVal(chooseautomation.getChooseAutomation());
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() { 
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
