/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author Rikhil
 */
public class DefensiveArm extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    public static Talon defense = new Talon(Constants.defense_motor.getInt());

    public void defense_up()
    {
        defense.set(1);
    }
    public void defense_down()
    {
        defense.set(-1);
    }
    public void defense_stop()
    {
        defense.set(0);
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
