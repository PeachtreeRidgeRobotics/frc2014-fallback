/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Compressor;
import com.prhsrobotics.frc2014.commands.CompressorStart;
/**
 *
 * @author RoboLions
 */
public class CompressorSystem extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    public static Compressor compressor = new Compressor(Constants.compressor_pressure_switch.getInt(), Constants.compressor_port.getInt());
    
    public void start_compressor()
    {
        this.compressor.start();
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new CompressorStart());
    }
}
