/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author RoboLions
 */
public class Intake extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    private Solenoid intake_a = new Solenoid(Constants.intake_solenoid_a.getInt());
    private Solenoid intake_b = new Solenoid(Constants.intake_solenoid_b.getInt());
    private Talon intake_motor = new Talon(Constants.intake_motor_port.getInt());
    
    public void draw_in()
    {
        intake_motor.set(1);
    }
    public void draw_out()
    {
        intake_motor.set(-1);
    }
    public void draw_stop()
    {
        intake_motor.set(0);
    }
    public void push()
    {
        intake_a.set(true);
    }
    public void push_stop()
    {
        intake_a.set(false);
    }
    public void pull()
    {
        intake_b.set(true);
    }
    public void pull_stop()
    {
        intake_b.set(false);
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
