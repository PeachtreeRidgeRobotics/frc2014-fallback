/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 * @author RoboLions
 */
public class Shooter extends Subsystem {
    private Solenoid solenoid_1 = new Solenoid(Constants.left_solenoid_a.getInt());
    private Solenoid solenoid_2 = new Solenoid(Constants.right_solenoid_b.getInt());
    private Solenoid solenoid_3 = new Solenoid(Constants.right_solenoid_a.getInt());
    private Solenoid solenoid_4 = new Solenoid(Constants.right_encoder_port_b.getInt());
    
    public void pull()
    {
        solenoid_1.set(true);
        solenoid_3.set(true);
    }
    public void pull_stop()
    {
        solenoid_1.set(false);
        solenoid_3.set(false);
    }
    public void push()
    {
        solenoid_2.set(true);
        solenoid_4.set(true);
    }
    public void push_stop()
    {
        solenoid_2.set(false);
        solenoid_4.set(false);
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
