/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc2014.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Talon;
import com.prhsrobotics.frc2014.commands.CompressorStart;
/**
 *
 * @author RoboLions
 */
public class Manipulator extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    public static Compressor compressor = new Compressor(14, 1);
    public static Talon defense = new Talon(6);
    public void start_compressor()
    {
        this.compressor.start();
    }
    public void defense_up()
    {
        defense.set(1);
    }
    public void defense_down()
    {
        defense.set(-1);
    }
    public void defense_stop()
    {
        defense.set(0);
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new CompressorStart());
    }
}
