package com.prhsrobotics.frc2014.subsystems;

import com.prhsrobotics.frc2014.Constants;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.*;
import com.prhsrobotics.frc2014.commands.ArcadeDrive;

/**
 * Controls for the chassis subsystem of the robot.
 * @author Nitant
 */
public class Chassis extends Subsystem {
    public Victor frontLeftDriveMotor = new Victor(Constants.front_left_drive_motor.getInt());
    public Victor rearLeftDriveMotor = new Victor(Constants.rear_left_drive_motor.getInt());
    public Victor frontRightDriveMotor = new Victor(Constants.front_right_drive_motor.getInt());
    public Victor rearRightDriveMotor = new Victor(Constants.rear_right_drive_motor.getInt());
        
    public RobotDrive drivetrain = new RobotDrive(frontLeftDriveMotor, rearLeftDriveMotor, frontRightDriveMotor, rearRightDriveMotor);
    public Chassis(String name) {
        super(name);
        
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
    }
    
    public Chassis() {
        super();
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
    }
    
    /**
     * Drives the robot using two sticks and the axis selected from each stick.
     * @param leftstick the left stick of the controller being used
     * @param leftaxis axis of left stick of controller
     * @param rightstick the right stick of the controller being used
     * @param rightaxis axis of right stick of controller
     */
    
    public void arcadeDrive(GenericHID leftstick , int leftaxis, GenericHID rightstick, int rightaxis){
        drivetrain.arcadeDrive(leftstick, leftaxis, rightstick, rightaxis);
    }
    
    /**
     * Moves the robot straight forward or backward.
     * @param power what power the robot should move with
     */
    public void driveStraight(double power) {
        drivetrain.drive(power, 0);
    }
    
    /**
     * Turns the motor left or right.
     * @param direction which direction the robot should move in (positive right, negative left)
     */
    public void turn(double direction) {
        drivetrain.drive(1, direction);
    }
    
    /**
     * Stops the robot.
     */
    public void brake() {
        drivetrain.drive(0, 0);
    }
    

    /**
     * Specifies no default command for the system.
     */
    protected void initDefaultCommand() {
        setDefaultCommand(new ArcadeDrive());
    }
}